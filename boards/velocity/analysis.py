# libraries
import os
import pandas as pd
# modules
from utilities import utilities as ut


############################# Leg Statistics #############################
def run_leg_statistics(prefix="perf"):
    """Process all leg files and extract sailing statistics on a DataFrame."""
    print("# Sailing stats processing...")
    legs_rows = []
    export_folder = "export"

     # Process each leg file
    for file_name in os.listdir(export_folder):
        if file_name.startswith('leg') and file_name.endswith('.csv'):
            file_path = os.path.join(export_folder, file_name)
            leg_stats = process_leg_statistics(file_path)
            legs_rows.append(leg_stats)
            
    # Create a DataFrame with all the data
    columns = [
        "leg", "state", "init_tms", "last_tms", "duration",
        "avr_vmg", "std_vmg", "avr_sog", "std_sog",
        "avr_twa", "std_twa", "avr_tws", "std_tws",
        "avr_twd", "mnv_num"
    ]
    data = pd.DataFrame(legs_rows, columns=columns)
    # file_name = f"{prefix}.csv"
    # data.to_csv(f"export/{file_name}", index=False)
    # print(f"- Leg Stats saved to {file_name}")
    return data


def process_leg_statistics(file_path):
    """Process a single leg file and extract required metrics."""
    df = pd.read_csv(file_path)
    file_name = os.path.basename(file_path)

    # Define states
    sailing_states = ["upwind", "downwind", "reaching"]
    maneuver_states = ["tacking", "jibing"]

    # Filter rows based on the `state` column
    df_sailing = df[df["state"].isin(sailing_states)]
    df_maneuver = df[df["state"].isin(maneuver_states)]

    # Extract leg number from filename
    leg = int(file_name.replace("leg", "").replace(".csv", ""))

    # Determine the most frequent state
    state = df["state"].mode()[0]

    # Get start and end timestamp
    init_tms = df["tms"].iloc[0]
    last_tms = df["tms"].iloc[-1]

    # Calculate duration in minutes:seconds
    duration_seconds = (pd.to_datetime(last_tms) - pd.to_datetime(init_tms)).total_seconds()
    duration = f"{int(duration_seconds // 60)}:{int(duration_seconds % 60):02}"

    # Sailing metrics
    avr_vmg = ut.mean_spd(df_sailing["vmg"]) if not df_sailing.empty else None
    std_vmg = ut.stdv_spd(df_sailing["vmg"]) if not df_sailing.empty else None
    avr_sog = ut.mean_spd(df_sailing["sog"]) if not df_sailing.empty else None
    std_sog = ut.stdv_spd(df_sailing["sog"]) if not df_sailing.empty else None
    avr_twa = ut.mean_twa(df_sailing["twa"]) if not df_sailing.empty else None
    std_twa = ut.stdv_twa(df_sailing["twa"]) if not df_sailing.empty else None
    avr_tws = ut.mean_spd(df_sailing["tws"]) if not df_sailing.empty else None
    std_tws = ut.stdv_spd(df_sailing["tws"]) if not df_sailing.empty else None
    avr_twd = ut.mean_twd(df_sailing["twd"]) if not df_sailing.empty else None
    mnv_num = len(df_maneuver) // 7  # Divide by 7 to account for rows per maneuver

    print(f"- Leg {leg} sailing processed")
    return [
        leg, state, init_tms, last_tms, duration,
        avr_vmg, std_vmg, avr_sog, std_sog,
        avr_twa, std_twa, avr_tws, std_tws, 
        avr_twd, mnv_num
    ]


############################# Maneuver Statistics #############################
def run_maneuver_statistics(prefix="mnvr"):
    """Process all leg files and extract maneuver statistics on a DataFrame."""
    print("# Maneuver stats processing...")
    mnvs_rows = []
    export_folder = "export"

     # Process each leg file
    for file_name in os.listdir(export_folder):
        if file_name.startswith('leg') and file_name.endswith('.csv'):
            file_path = os.path.join(export_folder, file_name)
            mnv_stats = process_maneuver_statistics(file_path)
            mnvs_rows.extend(mnv_stats)
            
    # Create a DataFrame with all the data
    columns = [
        "leg", "state", "tms", "avr_vmg", 
        "avr_sog", "avr_tws", "avr_twa", "std_cog"
    ]
    data = pd.DataFrame(mnvs_rows, columns=columns)
    # file_name = f"{prefix}.csv"
    # data.to_csv(f"export/{file_name}", index=False)
    # print(f"- Maneuver Stats saved to {file_name}")
    return data


def process_maneuver_statistics(file_path):
    """Process a single leg file and extract required metrics."""
    df = pd.read_csv(file_path)
    file_name = os.path.basename(file_path)

    # Define states
    maneuver_states = ["tacking", "jibing"]

    # Extract leg number from filename
    leg = int(file_name.replace("leg", "").replace(".csv", ""))

    # Filter rows based on the `state` column
    df_maneuver = df[df["state"].isin(maneuver_states)]

    maneuvers = []
    for i in range(0, len(df_maneuver), 7):
        segment = df_maneuver.iloc[i:i+7]
        if len(segment) == 7:
            tms = segment["tms"].iloc[0]
            state = segment["state"].iloc[0]
            avr_vmg = ut.mean_spd(segment["vmg"])
            avr_sog = ut.mean_spd(segment["sog"])
            avr_twa = ut.mean_twa(segment["twa"])
            avr_tws = ut.mean_spd(segment["tws"])
            std_cog = ut.stdv_twd(segment["cog"])

            maneuvers.append([
                leg, state, tms, avr_vmg, 
                avr_sog, avr_tws, avr_twa, std_cog
            ])

    print(f"- Leg {leg} maneuvers processed")
    return maneuvers
