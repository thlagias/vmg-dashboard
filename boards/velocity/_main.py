from streamlit import streamlit as st
from utilities import utilities as ut
from analytics import racing    as da
from boards.racing  import clean    as cl
from boards.racing   import widgets    as wg
from crews     import racecrew  as ai


def show_dashboard():
    col1, col2 = st.columns([2, 2])  # Adjust column width ratio as needed
    # Load CSS
    with open('static/styles.css') as f:
        st.markdown(f'<style>{f.read()}</style>', unsafe_allow_html=True)
    with col1:
        st.title("📊 Performance Analysis") # Set dashboard title
    with col2:
        racing_file = st.file_uploader("Upload csv file", key="racing_uploader")