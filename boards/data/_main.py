# modules
from streamlit import streamlit as st
from boards.data import widgets as wg


def show_dashboard():
    # Load CSS
    with open('static/styles.css') as f:
        st.markdown(f'<style>{f.read()}</style>', unsafe_allow_html=True)
    
    # Dashboard title
    st.markdown("<h1 style='text-align: center;'>📁 Data Import</h1>", unsafe_allow_html=True)
    
    # Initialize session state for selected race
    if "raceId" not in st.session_state:
        st.session_state.raceId = None

    # Show race ids
    st.markdown("<br><br>", unsafe_allow_html=True)
    raceId = wg.select_race_id()
    # Edit race data
    st.markdown("<br><br>", unsafe_allow_html=True)
    wg.edit_race_data(raceId)
    st.markdown("<br><br>", unsafe_allow_html=True)
    wg.edit_start_data(raceId)
    st.markdown("<br><br>", unsafe_allow_html=True)
    wg.edit_class_data(raceId)