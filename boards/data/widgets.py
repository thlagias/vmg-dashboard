# modules
from streamlit import streamlit as st
from utilities import network as nw
from pathlib import Path as Path
from pandas import pandas as pd
from boards.data.clean import race as race
from boards.data.clean import start as start
from boards.data.clean import wind as wind


def select_race_id():        
    cols = st.columns([4, 2, 2, 2, 2], gap="small")
    headers = ["Race ID", "Forecast", "Start", "Race", "Actions"]
    for col, header in zip(cols, headers):
        col.write(header)

    # Scrollable data rows
    with st.container():
        st.markdown("""
            <style>
            [data-testid="stVerticalBlock"] div:has(> [data-testid="stHorizontalBlock"]) {
                max-height: 500px;
                overflow-y: auto;
            }
            </style>
        """, unsafe_allow_html=True)
        
        # Display data rows
        races_data = nw.list_races()
        if not races_data:
            st.warning("No race data available")
            return None

        for race in races_data:
            raceId = race["raceId"]
            cols = st.columns([4, 2, 2, 2, 2], gap="small")
            
            # Display race information
            cols[0].write(raceId)
            cols[1].write("✅" if race.get("class", False) else "❌")
            cols[2].write("✅" if race.get("start", False) else "❌")
            cols[3].write("✅" if race.get("race", False) else "❌")
            
            # Create single button and handle its click
            if cols[4].button("Import Files", key=f"import_{raceId}", use_container_width=True):
                with st.spinner(f"Downloading files..."):
                    presigned_urls = nw.get_race_presigned_urls(raceId)
                    race_data_files = nw.get_race_files(presigned_urls)
                    if race_data_files:
                        st.success(f"Files downloaded successfully for race {raceId}")
                        st.session_state.raceId = raceId
                    else:
                        st.error(f"Error downloading files for race {raceId}")
                        st.session_state.raceId = None
                        
    return st.session_state.raceId


def edit_race_data(raceId):
    if raceId is not None:

        path = f"datasets/stage1/{raceId}-race-data.csv"
        
        with st.container():
            # Load the data only if it's not already in session state to avoid reloading
            if f"{raceId}_race_data" not in st.session_state:
                st.session_state[f"{raceId}_race_data"] = pd.read_csv(path)

            # Create editable data grid with dynamic rows
            data = st.session_state[f"{raceId}_race_data"]
            edit = st.data_editor(
                data, 
                height=550,
                num_rows="dynamic", 
                use_container_width=True,
                key=f"{raceId}_race_editor"
            )
            st.session_state[f"{raceId}_race_data"] = edit

        col1, col2 = st.columns([1, 1])

        # Add save button in first column
        with col1:
            save_button = st.button(
                "**Update Race Data**",
                use_container_width=True,
                key=f"{raceId}_race_save",
            )

        # Add clean button in second column 
        with col2:
            clean_button = st.button(
                "**Clean Race Data**",
                use_container_width=True,
                key=f"{raceId}_race_clean",
            )

        # Handle save button click - save data to CSV
        if save_button:
            st.session_state[f"{raceId}_race_data"].to_csv(path, index=False)
            st.rerun()

        # Handle clean button click (cleaning implementation pending)
        if clean_button:
            st.session_state[f"{raceId}_race_data"] = race.run_data_clean(path)
            st.session_state[f"{raceId}_race_data"].to_csv(path, index=False)
            st.rerun()



def edit_start_data(raceId):
    if raceId is not None:

        with st.container():
            path = f"datasets/stage1/{raceId}-start-data.csv"

            # Load CSV data into session state if not already present
            if f"{raceId}_start_data" not in st.session_state:
                st.session_state[f"{raceId}_start_data"] = pd.read_csv(path)

            # Create an editable data grid with dynamic rows
            data = st.session_state[f"{raceId}_start_data"]
            edit = st.data_editor(
                data,
                height=550,
                num_rows="dynamic",
                use_container_width=True,
                key=f"{raceId}_start_editor",
            )
            st.session_state[f"{raceId}_start_data"] = edit

        col1, col2 = st.columns([1, 1])

        # Add save button in first column
        with col1:
            save_button = st.button(
                "**Update Start Data**",
                use_container_width=True,
                key=f"{raceId}_start_save",
            )

        # Add clean button in second column
        with col2:
            clean_button = st.button(
                "**Clean Start Data**",
                use_container_width=True,
                key=f"{raceId}_start_clean",
            )

        # Handle save button click - save data to CSV
        if save_button:
            st.session_state[f"{raceId}_start_data"].to_csv(path, index=False)
            st.rerun()

        # Handle clean button click (cleaning implementation needed)
        if clean_button:
            st.session_state[f"{raceId}_start_data"] = start.run_data_clean(path)
            st.session_state[f"{raceId}_start_data"].to_csv(path, index=False)
            st.rerun()



def edit_class_data(raceId):
    if raceId is not None:

        with st.container():
            path = f"datasets/stage1/{raceId}-class-data.csv"

            # Load the data only if it's not already cached in session_state
            if f"{raceId}_class_data" not in st.session_state:
                st.session_state[f"{raceId}_class_data"] = pd.read_csv(path)

            # Create an editable data grid with dynamic rows
            data = st.session_state[f"{raceId}_class_data"]
            edit = st.data_editor(
                data, 
                height=550,
                num_rows="dynamic",
                use_container_width=True,
                key=f"{raceId}_class_editor"
            )
            st.session_state[f"{raceId}_class_data"] = edit

        col1, col2 = st.columns([1, 1])

        # Add save button in first column
        with col1:
            save_button = st.button(
                "**Update Wind Data**",
                use_container_width=True,
                key=f"{raceId}_class_save",
            )

        # Add clean button in second column
        with col2:
            clean_button = st.button(
                "**Clean Wind Data**",
                use_container_width=True,
                key=f"{raceId}_class_clean",
            )

        # Handle save button click - save data to CSV
        if save_button:
            st.session_state[f"{raceId}_class_data"].to_csv(path, index=False)
            st.success(f"Data saved as {raceId}-class-data.csv")

        # Handle clean button click (cleaning implementation pending)
        if clean_button:
            # TODO: implement data cleaning logic
            st.success(f"Data cleaned as {raceId}-class-data.csv")
