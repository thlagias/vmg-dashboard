# libraries
import pandas as pd
import ruptures as rpt
# modules
from utilities import utilities as ut


def load_dataset(data_path):
    """Load the dataset."""
    data = pd.read_csv(data_path)
    return data


def clean_dataset(data):
    """Preprocess the dataset."""
    data['tms'] = pd.to_datetime(data['tms'])
    # Clean 'cog' column: remove non-numeric characters and convert to float
    data['cog'] = pd.to_numeric(data['cog'].astype(str).str.extract('(\d+)')[0], errors='coerce')
    return data


def segment_analysis(data, column, model="rbf", penalty=10):
    """Perform PELT Time-Series segmentation on the specified column."""
    series = data[column]
    algo = rpt.Pelt(model=model).fit(series.values)
    breakpoints = algo.predict(pen=penalty)
    return breakpoints


def classify_maneuver(data, breakpoints):
    """Simplified state: Tacks, Jibes, and Mark Roundings as a catch-all."""
    states = []
    for bp in breakpoints:  # Breakpoints after filtering
        if bp >= len(data):  # Skip invalid breakpoints
            continue
        # Sample time slots before and after the breakpoint
        pre_start_time = data.iloc[bp]['tms'] - pd.Timedelta(seconds=5)
        pre_end_time = data.iloc[bp]['tms']
        post_start_time = data.iloc[bp]['tms']
        post_end_time = data.iloc[bp]['tms'] + pd.Timedelta(seconds=5)
        pre_window = data[(data['tms'] >= pre_start_time) & (data['tms'] < pre_end_time)]
        post_window = data[(data['tms'] >= post_start_time) & (data['tms'] <= post_end_time)]
        if len(pre_window) == 0 or len(post_window) == 0:
            states.append('rounding')  # Treat empty data windows as rounding
            continue
        # Compute mean TWA for pre and post windows
        pre_twa_mean = pre_window['twa'].mean()
        post_twa_mean = post_window['twa'].mean()
        # Classify based on TWA ranges
        if -60 <= pre_twa_mean <= 60 and -60 <= post_twa_mean <= 60:  # Upwind on both sides
            states.append('tacking')
        elif 135 <= abs(pre_twa_mean) <= 180 and 135 <= abs(post_twa_mean) <= 180:  # Downwind on both sides
            states.append('jibing')
        else:
            states.append('rounding')  # Catch-all for other transitions
    return states


def filter_noise_on_mark_roundings(data, breakpoints, states, delta_cog_threshold=20):
    """Filter out noise specifically for Mark Roundings based on COG stability."""
    filtered_breakpoints = []
    for i, bp in enumerate(breakpoints):
        # Ensure the index does not exceed the state list
        if i >= len(states):
            break
        if states[i] == 'rounding':
            pre_start_time = data.iloc[bp]['tms'] - pd.Timedelta(seconds=6)
            post_end_time = data.iloc[bp]['tms'] + pd.Timedelta(seconds=6)
            pre_window = data[(data['tms'] >= pre_start_time) & (data['tms'] < data.iloc[bp]['tms'])]
            post_window = data[(data['tms'] > data.iloc[bp]['tms']) & (data['tms'] <= post_end_time)]
            if len(pre_window) == 0 or len(post_window) == 0:
                continue
            # Compute mean COG before and after the breakpoint
            pre_cog_mean = pre_window['cog'].mean()
            post_cog_mean = post_window['cog'].mean()
            delta_cog = abs(post_cog_mean - pre_cog_mean)
            # Retain Mark Roundings only if COG changes sufficiently
            if delta_cog > delta_cog_threshold:
                filtered_breakpoints.append(bp)
        else:
            # Retain all other states (Tacks and Jibes)
            filtered_breakpoints.append(bp)
    return filtered_breakpoints


def split_into_legs(data, breakpoints):
    """Split the dataset into legs based on Mark Rounding breakpoints."""
    legs = []
    start_idx = 0
    for bp in breakpoints:
        legs.append(data.iloc[start_idx:bp].copy())
        start_idx = bp
    legs.append(data.iloc[start_idx:].copy())  # Add the last leg
    return legs


def classify_legs(legs):
    """Classify legs as Upwind, Downwind, or Reaching based on TWA."""
    leg_classifications = []
    for leg in legs:
        mean_twa = leg['twa'].mean()
        if -60 <= mean_twa <= 60:
            leg_classifications.append('upwind')
        elif 135 <= abs(mean_twa) <= 180:
            leg_classifications.append('downwind')
        else:
            leg_classifications.append('reaching')
        # Assign the state to all rows in the leg
        leg['state'] = leg_classifications[-1]
    return legs


def combine_classifications(legs, breakpoints, maneuver_classifications):
    """Combine legs and maneuver states for the full dataset."""
    combined_data = pd.concat(legs)
    combined_data = combined_data.sort_index()
    # Map maneuver states to the full dataset
    maneuver_mapping = {bp: cls for bp, cls in zip(breakpoints, maneuver_classifications)}
    maneuver_series = pd.Series(combined_data.index.map(maneuver_mapping), index=combined_data.index)
    combined_data['state'] = maneuver_series.combine_first(combined_data['state'])
    return combined_data.drop(columns=['Leg state'], errors='ignore')


def extend_maneuver_classifications(data, breakpoints, states):
    """Extend maneuver states to 3 timestamps before and after detected breakpoints."""
    extended_classifications = data['state'].copy()
    for i, bp in enumerate(breakpoints):
        maneuver = states[i]
        # Extend state to 3 timestamps before and after the breakpoint
        start_idx = max(bp - 3, 0)
        end_idx = min(bp + 3, len(data) - 1)
        extended_classifications.loc[start_idx:end_idx] = maneuver
    data['state'] = extended_classifications
    return data


def export_race(load_data, data):
    """Export the results with states to a CSV file."""
    load_data['state'] = None  # Add a state column
    load_data.loc[data.index, 'state'] = data['state']
    load_data = ut.add_continuous_timer(load_data)
    return load_data


def split_into_rounding_legs(data):
    """Split dataset into legs where each leg starts at the first 'rounding' and ends at the next distinct 'rounding'."""
    legs = []
    start_idx = 0  # Start from the beginning of the dataset
    in_rounding = False

    for i, row in data.iterrows():
        if row['state'] == 'rounding':
            if not in_rounding:  # Start of a rounding block
                if start_idx < i:  # Add the leg leading to this rounding
                    legs.append(data.iloc[start_idx:i].copy())
                start_idx = i  # Update start index for the next leg
                in_rounding = True
        else:
            in_rounding = False  # End of a rounding block

    # Add any remaining data as the final leg
    if start_idx < len(data):
        legs.append(data.iloc[start_idx:].copy())

    return legs


def process_and_export_legs(data, prefix="leg"):
    """Process the final export.csv and export each leg starting and ending at rounding timestamps."""
    legs = split_into_rounding_legs(data)
    for i, leg in enumerate(legs):
        file_name = f"{prefix}{i+1}.csv"
        leg.to_csv(f"datasets/stage2/{file_name}", index=False)
        print(f"- Leg {i+1} saved to {file_name}")


def run_data_clean(import_file):
    print("# Race Cleaning...")
    load_data = load_dataset(import_file)
    clean_data = clean_dataset(load_data.copy())
    # Perform segmentation
    breakpoints_twa = segment_analysis(clean_data, column='twa')
    # Classify maneuvers
    states = classify_maneuver(clean_data, breakpoints_twa)
    # Filter out noise specifically for Mark Roundings
    filtered_breakpoints = filter_noise_on_mark_roundings(clean_data, breakpoints_twa, states)
    # Split into legs and classify them
    legs = split_into_legs(clean_data, filtered_breakpoints)
    classified_legs = classify_legs(legs)
    # Combine results
    combined_data = combine_classifications(classified_legs, filtered_breakpoints, states)
    # Extend maneuver states
    extended_data = extend_maneuver_classifications(combined_data, filtered_breakpoints, states)
    # Save final dataset
    extended_data = export_race(load_data, extended_data)
    # Process and export legs as individual CSV files
    process_and_export_legs(extended_data)
    return extended_data
