# libraries
import pandas as pd
import ruptures as rpt
# modules
from utilities import utilities as ut


def load_dataset(data_path):
    """Load the dataset."""
    data = pd.read_csv(data_path)
    return data


def clean_dataset(data):
    """Preprocess the dataset."""
    data['tms'] = pd.to_datetime(data['tms'])
    return data


def segment_analysis(data, column, model="rbf", penalty=7):
    """Perform PELT Time-Series segmentation on the specified column."""
    series = data[column]
    algo = rpt.Pelt(model=model).fit(series.values)
    breakpoints = algo.predict(pen=penalty)
    # Filter out breakpoints after gun time
    breakpoints = [bp for bp in breakpoints if bp <= 302]
    return breakpoints


def classify_maneuver(data, breakpoints):
    """Simplified state: Tacks, Jibes, and Mark Roundings as a catch-all."""
    states = []
    for bp in breakpoints:  # Breakpoints after filtering
        if bp >= len(data):  # Skip invalid breakpoints
            continue
        # Sample time slots before and after the breakpoint
        pre_start_time = data.iloc[bp]['tms'] - pd.Timedelta(seconds=2)
        pre_end_time = data.iloc[bp]['tms']
        post_start_time = data.iloc[bp]['tms']
        post_end_time = data.iloc[bp]['tms'] + pd.Timedelta(seconds=2)
        pre_window = data[(data['tms'] >= pre_start_time) & (data['tms'] < pre_end_time)]
        post_window = data[(data['tms'] >= post_start_time) & (data['tms'] <= post_end_time)]
        if len(pre_window) == 0 or len(post_window) == 0:
            continue
        # Compute mean TWA for pre and post windows
        pre_twa_mean = pre_window['twa'].mean()
        post_twa_mean = post_window['twa'].mean()
        # Classify based on TWA ranges
        if -80 <= pre_twa_mean <= 80 and -80 <= post_twa_mean <= 80:  # Upwind on both sides
            states.append('tacking')
        elif 100 <= abs(pre_twa_mean) <= 180 and 100 <= abs(post_twa_mean) <= 180:  # Downwind on both sides
            states.append('jibing')
        else: 
            states.append('steering')
    return states


def split_into_phases(data, breakpoints, states):
    """Split race start into phases based on maneuver points."""
    phases = []
    maneuvers = []
    
    for bp, state in zip(breakpoints, states):
        if state in ('tacking', 'jibing'):
            maneuvers.append(bp)
    
    # Not enough maneuvers for proper phase splitting
    if len(maneuvers) < 2: return [data]
    # Define phase transition points
    phase1_to_phase2 = maneuvers[-2] # Second latest maneuver
    phase2_to_phase3 = maneuvers[-1] # First latest maneuver
    # Define phase data points
    phases.append(data.iloc[:phase1_to_phase2].copy())
    phases.append(data.iloc[phase1_to_phase2:phase2_to_phase3].copy())
    phases.append(data.iloc[phase2_to_phase3:].copy())
    return phases


def classify_phases(phases):
    """Classify phases including detection of holding phase."""
    phase_count = 0
    phase_hold  = None
    phase_accel = None
    phase_start = None

    for phase in phases:
        if phase_count == 0:
            phase['state'] = 'planning'
        if phase_count == 1:
            phase['state'] = 'positioning'
        if phase_count == 2:
            # Split the third phase into holding and acceleration based on ttb threshold
            ttb_seconds = pd.to_timedelta(phase['ttb'].astype(str).apply(ut.hhmmss)).dt.total_seconds()
            tmr_seconds = pd.to_timedelta(phase['timer'].astype(str).apply(ut.hhmmss)).dt.total_seconds()
            ttb_mask = ttb_seconds > 10
            tmr_mask = tmr_seconds > -1
            # Create holding phase
            phase_hold = phase[ttb_mask & ~tmr_mask].copy()
            phase_hold['state'] = 'holding'
            # Create acceleration phase
            phase_accel = phase[~ttb_mask & ~tmr_mask].copy()
            phase_accel['state'] = 'acceleration'
            # Create start phase
            phase_start = phase[~ttb_mask & tmr_mask].copy()
            phase_start['state'] = 'starting'
        phase_count += 1

    phases.pop()  # Remove the last phase
    if phase_hold is not None: phases.append(phase_hold)
    if phase_accel is not None: phases.append(phase_accel)
    if phase_start  is not None: phases.append(phase_start)
    return phases


def combine_classifications(phases, breakpoints, maneuver_classifications):
    """Combine phases with maneuver states."""
    combined_data = pd.concat(phases)
    combined_data = combined_data.sort_index()
    # Map maneuver states to the full dataset
    maneuver_mapping = {bp: cls for bp, cls in zip(breakpoints, maneuver_classifications)}
    maneuver_series = pd.Series(combined_data.index.map(maneuver_mapping), index=combined_data.index)
    combined_data['state'] = maneuver_series.combine_first(combined_data['state'])
    return combined_data


def extend_maneuver_classifications(data, breakpoints, states):
    """Extend maneuver states to 3 timestamps before and after detected breakpoints."""
    extended_classifications = data['state'].copy()
    for i, bp in enumerate(breakpoints):
        maneuver = states[i]
        # Extend state to 3 timestamps before and after the breakpoint
        start_idx = max(bp - 2, 0)
        end_idx = min(bp + 2, len(data) - 1)
        extended_classifications.loc[start_idx:end_idx] = maneuver
    data['state'] = extended_classifications
    return data


def export_race(load_data, data):
    """Export the results with states to a CSV file."""
    load_data['state'] = None  # Add a state column
    load_data.loc[data.index, 'state'] = data['state']
    return load_data


def run_data_clean(import_file):
    print("# Start Cleaning...")
    load_data = load_dataset(import_file)
    clean_data = clean_dataset(load_data.copy())
    # Perform segmentation
    breakpoints_twa = segment_analysis(clean_data, column='twa')
    # Classify maneuvers
    states = classify_maneuver(clean_data, breakpoints_twa)
    # Split into phases and classify them
    phases = split_into_phases(clean_data, breakpoints_twa, states)
    classified_phases = classify_phases(phases)
    # Combine results
    combined_data = combine_classifications(classified_phases, breakpoints_twa, states)
    # Extend maneuver states
    extended_data = extend_maneuver_classifications(combined_data, breakpoints_twa, states)
    # Save final dataset
    extended_data = export_race(load_data, extended_data)
    return extended_data
