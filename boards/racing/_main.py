# modules
from streamlit import streamlit as st
from utilities import utilities as ut
from boards.racing import widgets as wg
from boards.racing import analysis as da


def show_dashboard():
    # Load CSS
    with open('static/styles.css') as f:
        st.markdown(f'<style>{f.read()}</style>', unsafe_allow_html=True)

    # Dashboard title
    st.markdown("<h1 style='text-align: center;'>🏁 Race Overview</h1>", unsafe_allow_html=True)    
    
    if st.session_state.raceId is not None:
        try:
            # race file path
            raceId = st.session_state.raceId
            path = f"datasets/stage1/{raceId}-race-data.csv"
            # race dataframe
            race_df = ut.load_data(path)
            # race dictionary
            race_dc = da.race_summary(race_df)
            # gannt dictionary
            gnnt_dc = da.race_timeline(race_df)
            
            # race metrics
            col1, col2, col3, col4, col5 = st.columns(5)
            with col1:
                wg.twd_metric(race_dc)
            with col2:
                wg.tws_metric(race_dc)
            with col3:
                wg.tack_num_metric(race_dc)
            with col4:
                wg.sog_metric(race_dc)
            with col5:
                wg.state_perc_metric(race_dc)

            # race gannt
            with st.container():
                wg.race_gantt(gnnt_dc)

            # race plots
            col6, col7 = st.columns(2)
            with col6:
                wg.vmg_plot(race_df)
                wg.trc_plot(race_df)
            with col7:
                wg.sog_plot(race_df)
                wg.twa_plot(race_df)
                wg.tws_plot(race_df)
                wg.twd_plot(race_df)

            # race stats
            col8, col9 = st.columns(2)
            with col8:
                wg.race_stat(race_df)
            with col9:
                wg.race_corr(race_df)

            # race agents
            # ai.kickoff()
        except Exception as e:
            print(f"An error occurred: {e}")
