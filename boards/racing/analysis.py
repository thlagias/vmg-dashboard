# libraries
import pandas as pd
# modules
from utilities import utilities as ut


############################# Race Summary #############################
def race_summary(df):
    """Extract overall race metrics."""
    print("# Race Summary...")

    # Define states
    sailing_states = ["upwind", "downwind", "reaching"]
    maneuver_states = ["tacking", "jibing"]

    # Calculate metrics
    total_maneuvers = len(df[df["state"].isin(maneuver_states)]) // 7
    state_counts = df["state"].value_counts(normalize=True) * 100
    twd = [float(x.replace('°', '')) for x in df["twd"].tolist()]
    avr_sog = ut.mean_spd(df["sog"])
    avr_tws = ut.mean_spd(df["tws"])
    avr_twd = ut.mean_dir(twd)

    race_summary = {
        "total_maneuvers": total_maneuvers,
        "upwind_pct": state_counts.get("upwind", 0),
        "downwind_pct": state_counts.get("downwind", 0),
        "reaching_pct": state_counts.get("reaching", 0),
        "avr_sog": avr_sog,
        "avr_tws": avr_tws,
        "avr_twd": avr_twd
    }

    return race_summary


############################# Race Timeline #############################
def race_timeline(df):
    """
    Extract key breakpoints of states to visualize the race timeline.
    Returns a DataFrame or dictionary for Gantt chart visualization.
    """
    print("# Race Timeline...")

    # Ensure time is in datetime format for processing
    df['tms'] = pd.to_datetime(df['tms'])

    # Identify breakpoints where state changes
    df['state_change'] = df['state'].ne(df['state'].shift())
    breakpoints = df[df['state_change']].copy()

    # Calculate duration for each state segment
    breakpoints['duration'] = breakpoints['tms'].diff().shift(-1)

    # Fill last segment duration with a fixed value (end of race to last segment)
    final_duration = df['tms'].iloc[-1] - breakpoints['tms'].iloc[-1]
    breakpoints.loc[breakpoints.index[-1], 'duration'] = final_duration

    # Prepare data for visualization
    timeline_data = {
        'leg': [],
        'state': [],
        'start_time': [],
        'duration': []
    }

    for idx, row in breakpoints.iterrows():
        timeline_data['leg'].append(row['leg']) if 'leg' in df.columns else timeline_data['leg'].append(0)
        timeline_data['state'].append(row['state'])
        timeline_data['start_time'].append(row['tms'])
        timeline_data['duration'].append(row['duration'])

    # Convert to DataFrame for easy visualization
    timeline_df = pd.DataFrame(timeline_data)
    timeline_df['end_time'] = timeline_df['start_time'] + timeline_df['duration']

    return timeline_df 
