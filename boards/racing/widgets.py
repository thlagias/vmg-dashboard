# https://plotly.com/python-api-reference/
from plotly    import graph_objects as go
from plotly    import express       as px
from streamlit import streamlit     as st
from utilities import utilities     as ut


# metrics
def twd_metric(data):
    try:
        val = data["avr_twd"]
        fig = go.Figure(go.Indicator(
            value  = val,
            mode   = "number",
            title  = {'text': "TWD (°)"},
            domain = {'row': 0, 'column': 1},
        ))
        fig.update_layout(height=300)
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"TWD metric threw an error: {e}")


def tws_metric(data):
    try:
        val = data["avr_tws"]
        fig = go.Figure(go.Indicator(
            value  = val,
            mode   = "number",
            title  = {'text': "TWS (kts)"},
            domain = {'row': 0, 'column': 1},
            number = {'valueformat': '.1f'}
        ))
        fig.update_layout(height=300)
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"TWS metric threw an error: {e}")


def tack_num_metric(data):
    try:
        val = data["total_maneuvers"]
        fig = go.Figure(go.Indicator(
            value  = val,
            mode   = "number",
            title  = {'text': "Tacks (N)"},
            domain = {'row': 0, 'column': 1},
        ))
        fig.update_layout(height=300)
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"Tack number metric threw an error: {e}")


def sog_metric(data):
    try:
        val = data["avr_sog"]
        fig = go.Figure()
        fig.add_trace(go.Indicator(
            mode="gauge+number",
            title={'text': "SOG (kts)"},
            value=val,
            gauge={
                'axis': {'range': [0, 15]},
                'steps': [
                    {'range': [0, val], 'color': "green"},
                    {'range': [val, 15], 'color': "red"}
                ]
            }
        ))
        fig.update_layout(height=300)
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"SOG metric threw an error: {e}")


def state_perc_metric(data):
    try:
        up_val = data["upwind_pct"]
        dn_val = data["upwind_pct"] + data["downwind_pct"]
        fig = go.Figure()
        fig.add_trace(go.Indicator(
            mode="gauge",
            title={'text': "Legs (%)"},
            gauge={
                'axis': {'range': [0, 100]},
                'steps': [
                    {'range': [0, up_val], 'color': "green"},
                    {'range': [up_val, dn_val], 'color': "blue"},
                    {'range': [dn_val, 100], 'color': "red"}
                ]
            }
        ))
        fig.add_annotation(
            x=0.5,
            y=-0.2,
            text=(
                f"U: {data['upwind_pct']:.1f}% | "
                f"D: {data['downwind_pct']:.1f}% | "
                f"R: {data['reaching_pct']:.1f}%"
            ),
            showarrow=False,
            font=dict(size=14)
        )
        fig.update_layout(height=300)
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"State percentage metric threw an error: {e}")



# plots
def vmg_plot(dataframe):
    try:
        dat = dataframe.copy()
        fig = px.line(
            dat,
            x = "timer",
            y = "vmg",
            title = "VMG",
            range_x = [0, 1200],
            color_discrete_sequence = ['#34495E']
        )
        fig.update_layout(
            height = 350,
            xaxis_title = "Time (HH:MM:SS)",
            yaxis_title = "VMG (kts)",
            xaxis=dict(
                dtick=120,
                showline=True,
                linecolor='black',
                linewidth=1,
            ),
            yaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1,
            )
        )
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"VMG threw an error: {e}")


def sog_plot(dataframe):
    try:
        dat = dataframe.copy()
        fig = px.line(
            dat,
            x = "timer",
            y = "sog",
            title = "SOG",
            range_x = [0, 1200],
            color_discrete_sequence = ['#2E86C1']
        )
        fig.update_layout(
            height = 350,
            xaxis_title = "Time (HH:MM:SS)",
            yaxis_title = "SOG (kts)",
            xaxis=dict(
                dtick=120,
                showline=True,
                linecolor='black',
                linewidth=1,
            ),
            yaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1,
            )
        )
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"SOG threw an error: {e}")


def twa_plot(dataframe):
    try:
        dat = dataframe.copy()
        fig = px.line(
            dat,
            x = "timer",
            y = "twa",
            title = "TWA",
            range_x = [0, 1200],
            color_discrete_sequence = ['#27AE60']
        )
        fig.update_layout(
            height = 350,
            xaxis_title = "Time (HH:MM:SS)",
            yaxis_title = "VMG (kts)",
            xaxis=dict(
                dtick=120,
                showline=True,
                linecolor='black',
                linewidth=1,
            ),
            yaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1,
            )
        )
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"VMG threw an error: {e}")


def tws_plot(dataframe):
    try:
        dat = dataframe.copy()
        fig = px.line(
            dat,
            x = "timer",
            y = "tws",
            title = "TWS",
            range_x = [0, 1200],
            color_discrete_sequence = ['#8E44AD']
        )
        fig.update_layout(
            height = 350,
            xaxis_title = "Time (HH:MM:SS)",
            yaxis_title = "TWS (kts)",
            xaxis=dict(
                dtick=120,
                showline=True,
                linecolor='black',
                linewidth=1,
            ),
            yaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1,
            )
        )
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"TWS threw an error: {e}")


def twd_plot(dataframe):
    try:
        dat = dataframe.copy()
        fig = px.line(
            dat,
            x = "timer",
            y = "twd",
            title = "TWD",
            range_x = [0, 1200],
            color_discrete_sequence = ['#E67E22']
        )  
        fig.update_layout(
            height = 350,
            xaxis_title = "Time (HH:MM:SS)",
            yaxis_title = "TWD (°)",
            xaxis=dict(
                dtick=120,
                showline=True,
                linecolor='black',
                linewidth=1,
            ),
            yaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1,
            )
        )
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"TWD threw an error: {e}")


def race_gantt(dataframe):
    try:
        # Define the desired order of states
        state_order = [
            "jibing",
            "tacking",
            "rounding",
            "reaching",
            "downwind",
            "upwind"
        ]

        fig = px.timeline(
            dataframe,
            x_start="start_time",
            x_end="end_time",
            y="state",
            color="state",
            title="Timeline",
        )
        fig.update_yaxes(
            categoryorder="array",
            categoryarray=state_order
        )
        fig.update_layout(
            xaxis_title="Time",
            yaxis_title="State",
            showlegend=False,
            height=300
        )
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"GANTT threw an error: {e}")


def race_stat(dataframe):
    """Display key race statistics with formatted output."""
    try:        
        df_clean = dataframe.copy()
        df_clean['twd'] = df_clean['twd'].str.replace('°', '').astype(int)
        relevant = df_clean[["tws", "twd", "twa", "sog", "vmg", "accel", "xpt", "ypt"]]
        statistics = relevant.describe().round(1)
        st.markdown("<h5>Stat</h5>", unsafe_allow_html=True)
        st.dataframe(
            statistics,
            height=310,
            use_container_width=True
        )
    except Exception as e:
        st.error(f"Error displaying statistics: {e}")


def race_corr(dataframe):
    """Display correlation matrix with improved visualization."""
    try:
        df_clean = dataframe.copy()
        df_clean['twd'] = df_clean['twd'].str.replace('°', '').astype(int)
        relevant = df_clean[["tws", "twd", "twa", "sog", "vmg", "accel", "xpt", "ypt"]]
        correlation = relevant.corr().round(2)
        st.markdown("<h5>Corr</h5>", unsafe_allow_html=True)
        st.dataframe(
            correlation,
            height=310,
            use_container_width=True
        )
    except Exception as e:
        st.error(f"Error displaying correlation matrix: {e}")


def trc_plot(dataframe):
    """
    Create an animated plot showing boat position and trace
    Args:
        dataframe: DataFrame with columns ['timer', 'x', 'y']
    """
    try:
        # Downsample the data to reduce size (e.g., take every 5th row)
        sample_rate = 5  # Adjust this value to balance between detail and performance
        dat = dataframe.iloc[::sample_rate].copy()
        fig = go.Figure()
        
        startline_length = 300
        
        # Add the starting line
        fig.add_trace(go.Scatter(
            x=[-startline_length/2, +startline_length/2],
            y=[0, 0],
            mode='lines',
            name='Starting Line',
            line=dict(color='black', width=3),
            hoverinfo='skip'
        ))

        if {'xpt', 'ypt'}.issubset(dat.columns):
            # Add boat trace (historical path)
            fig.add_trace(go.Scatter(
                x=dat['xpt'],
                y=dat['ypt'],
                mode='lines',
                name='Boat Trace',
                line=dict(color='blue', width=1),
                opacity=0.5
            ))
            # Add animated boat position
            fig.add_trace(go.Scatter(
                x=dat['xpt'],
                y=dat['ypt'],
                mode='markers',
                name='Position',
                marker=dict(
                    symbol='diamond-tall',
                    size=16,
                    color='navy',
                    angle=0,  # Initial angle
                ),
                hovertemplate=(
                    '<b>Time: %{customdata[0]}</b><br>' +
                    '<b>Timer: %{customdata[1]}</b><br>' +
                    '<b>Position:</b><br>' +
                    '  X: %{x:.1f}m<br>' +
                    '  Y: %{y:.1f}m<br>' +
                    '<b>Metrics:</b><br>' +
                    '  SOG: %{customdata[2]:.1f}kts<br>' +
                    '  COG: %{customdata[3]}°<br>' +
                    '  TWA: %{customdata[4]:.0f}°<br>' +
                    '  TWD: %{customdata[5]}°<br>' +
                    '  TWS: %{customdata[6]:.1f}kts<br>' +
                    '  VMG: %{customdata[7]:.1f}kts<br>' +
                    '<extra></extra>'
                ),
                customdata=list(zip(
                    dat['tms'],
                    dat['timer'],
                    dat['sog'],
                    [ut.format_dir(x) for x in dat['cog']],
                    dat['twa'],
                    [ut.format_dir(x) for x in dat['twd']],
                    dat['tws'],
                    dat['vmg']
                ))
            ))

        # Add pin end marker (red triangle)
        fig.add_trace(go.Scatter(
            x=[-startline_length/2],
            y=[0],
            mode='markers',
            name='Pin End',
            marker=dict(
                symbol='triangle-up',
                size=16,
                color='red',
            ),
            hoverinfo='skip'
        ))

        # Add starboard end marker (green triangle)
        fig.add_trace(go.Scatter(
            x=[startline_length/2],
            y=[0],
            mode='markers',
            name='Stb End',
            marker=dict(
                symbol='triangle-up',
                size=16,
                color='green',
            ),
            hoverinfo='skip'
        ))

        # Update layout for racing view
        fig.update_layout(
            title="Position",
            height=1050,
            showlegend=True,
            xaxis=dict(
                title="X (m)",
                range=[-500, +500],  # Wider range for tacks
                showline=True,
                linecolor='black',
                linewidth=1
            ),
            yaxis=dict(
                title="Y (m)",
                range=[-20, +1000],  # Much longer range for upwind/downwind legs
                showline=True,
                linecolor='black',
                linewidth=1
            ),
            # Fixed slider configuration
            sliders=[dict(
                active=0,
                yanchor='top',
                xanchor='left',
                currentvalue=dict(
                    font=dict(size=16),
                    prefix='Time: ',
                    visible=True,
                    xanchor='right',
                ),
                pad=dict(b=10, t=50),
                len=0.9,
                x=0.1,
                y=0,
                tickwidth=0,
                steps=[dict(
                    args=[[f'frame{k}'],
                        dict(mode='immediate',
                            frame=dict(duration=100, redraw=True),
                            transition=dict(duration=0))
                        ],
                    label=str(dataframe['timer'].iloc[k*sample_rate]),
                    method='animate',
                ) for k in range(len(dat))]
            )],
            
            # Play/pause buttons
            updatemenus=[
                dict(
                    type='buttons',
                    showactive=True,
                    x=+0.07,
                    y=-0.04,
                    yanchor='top',
                    direction='right',
                    pad=dict(r=10),
                    bgcolor='#FFFFFF',
                    bordercolor='#000000',
                    font=dict(size=14, color='#000000'),
                    buttons=[
                        dict(
                            label='▶     Play   ',
                            method='animate',
                            args=[None, dict(
                                frame=dict(
                                    duration=100,
                                    redraw=True
                                ),
                                fromcurrent=True,
                                mode='immediate',
                                transition=dict(
                                    duration=0,
                                    easing='linear'
                                )
                            )],
                        ),
                    ],
                ),
                dict(
                    type='buttons',
                    showactive=True,
                    x=+0.07,
                    y=-0.08,
                    yanchor='top',
                    direction='right',
                    pad=dict(r=10),
                    bgcolor='#FFFFFF',
                    bordercolor='#000000',
                    font=dict(size=14, color='#000000'),
                    buttons=[
                        dict(
                            label='⏸  Pause  ',
                            method='animate',
                            args=[[None], dict(
                                frame=dict(duration=0, redraw=False),
                                mode='immediate',
                                transition=dict(duration=0)
                            )],
                        ),
                    ],
                )
            ]
        )

        # Create animation frames
        if {'xpt', 'ypt'}.issubset(dat.columns):
            fig.frames = [
                go.Frame(
                    data=[
                        # Starting line (static)
                        go.Scatter(
                            x=[-startline_length/2, startline_length/2],
                            y=[0, 0],
                            mode='lines',
                            name='Starting Line',
                            line=dict(color='black', width=3),
                            hoverinfo='skip'
                        ),
                        # Historical trace up to current time
                        go.Scatter(
                            x=dat['xpt'][:k+1],
                            y=dat['ypt'][:k+1],
                            mode='lines',
                            name='Boat Trace',
                            line=dict(color='blue', width=1),
                            opacity=0.5
                        ),
                        # Current boat position
                        go.Scatter(
                            x=[dat['xpt'].iloc[k]],
                            y=[dat['ypt'].iloc[k]],
                            mode='markers',
                            name='Position',
                            marker=dict(
                                symbol='diamond-tall',
                                size=16,
                                color='navy',
                                angle=ut.calculate_boat_angle(dat['xpt'].values, dat['ypt'].values, k),
                            ),
                            hovertemplate=(
                                '<b>Time: %{customdata[0]}</b><br>' +
                                '<b>Timer: %{customdata[1]}</b><br>' +
                                '<b>Position:</b><br>' +
                                '  X: %{x:.1f}m<br>' +
                                '  Y: %{y:.1f}m<br>' +
                                '<b>Metrics:</b><br>' +
                                '  SOG: %{customdata[2]:.1f}kts<br>' +
                                '  COG: %{customdata[3]}°<br>' +
                                '  TWA: %{customdata[4]:.0f}°<br>' +
                                '  TWD: %{customdata[5]}°<br>' +
                                '  TWS: %{customdata[6]:.1f}kts<br>' +
                                '  VMG: %{customdata[7]:.1f}kts<br>' +
                                '<extra></extra>'
                            ),
                            customdata=[[
                                dat['tms'].iloc[k],
                                dat['timer'].iloc[k],
                                dat['sog'].iloc[k],
                                ut.format_dir(dat['cog'].iloc[k]),
                                dat['twa'].iloc[k],
                                ut.format_dir(dat['twd'].iloc[k]),
                                dat['tws'].iloc[k],
                                dat['vmg'].iloc[k]
                            ]]
                        ),
                        # Pin end marker
                        go.Scatter(
                            x=[-startline_length/2],
                            y=[0],
                            mode='markers',
                            name='Pin End',
                            marker=dict(
                                symbol='triangle-up',
                                size=16,
                                color='red',
                            ),
                            hoverinfo='skip'
                        ),
                        # Starboard end marker
                        go.Scatter(
                            x=[startline_length/2],
                            y=[0],
                            mode='markers',
                            name='Stb End',
                            marker=dict(
                                symbol='triangle-up',
                                size=16,
                                color='green',
                            ),
                            hoverinfo='skip'
                        ),
                    ],
                    name=f'frame{k}'
                )
                for k in range(len(dat))
            ]

        # Display in Streamlit
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"Trace plot threw an error: {e}")
        st.error(f"Unable to display trace plot: {e}")
