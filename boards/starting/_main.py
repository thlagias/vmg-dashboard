# modules
from streamlit import streamlit as st
from utilities import utilities as ut
from boards.starting import widgets as wg
from boards.starting import analysis as da


def show_dashboard():
    # Load CSS
    with open('static/styles.css') as f:
        st.markdown(f'<style>{f.read()}</style>', unsafe_allow_html=True)

    # Dashboard title
    st.markdown("<h1 style='text-align: center;'>🚩 Start Analysis</h1>", unsafe_allow_html=True)

    if st.session_state.raceId is not None:
        try:
            # start file path
            raceId = st.session_state.raceId
            path = f"datasets/stage1/{raceId}-start-data.csv"
            # start dataframe
            start_df = ut.load_data(path)
            # start dictionary
            start_dc = da.start_summary(start_df)
            # gannt dictionary
            gnnt_dc = da.start_timeline(start_df)

            # start metrics
            col1, col2, col3, col4, col5 = st.columns(5)
            with col1:
                wg.lbs_metric(start_dc)
            with col2:
                wg.ptl_metric(start_dc)
            with col3:
                wg.ttb_metric(start_dc)
            with col4:
                wg.dtb_metric(start_dc)
            with col5:
                wg.sog_metric(start_dc)

            # start gannt
            with st.container():
                wg.start_gantt(gnnt_dc)

            # speed & distance plots
            col6, col7 = st.columns(2)
            with col6:
                wg.sog_plot(start_df)
            with col7:
                wg.dtl_plot(start_df)

            # burn plots
            col8, col9 = st.columns(2)
            with col8:
                wg.ttb_plot(start_df)
            with col9:
                wg.dtb_plot(start_df)

            # trace plots
            with st.container():
                wg.trc_plot(start_df)
    
            # start agents
            # ai.kickoff()
        except Exception as e:
            print(f"An error occurred: {e}")
