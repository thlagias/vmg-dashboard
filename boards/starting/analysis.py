# libraries
import pandas as pd
# modules
from utilities import utilities as ut


############################# Start Summary #############################
def start_summary(df):
    """Extract overall start metrics."""
    print("# Start Summary...")

    # Calculate metrics
    sog = df.loc[df['timer'] == "00:00", 'sog'].values[0]
    ptl = df.loc[df['timer'] == "00:00", 'ptl'].values[0]
    dtb = df.loc[df['timer'] == "00:00", 'dtb'].values[0]
    ttb = df.loc[df['timer'] == "00:00", 'ttb'].values[0]
    ttb = ut.start_timer_to_seconds(ttb)
    lbs = df.loc[df['timer'] == "00:00", 'lbs'].values[0]
    lbs = int(lbs.replace('°', ''))

    start_summary = {
        "lbs": lbs,
        "ptl": ptl,
        "ttb": ttb,
        "dtb": dtb,
        "sog": sog
    }

    return start_summary


############################# Start Timeline #############################
def start_timeline(df):
    """
    Extract key breakpoints of states to visualize the start timeline.
    Returns a DataFrame or dictionary for Gantt chart visualization.
    """
    print("# Start Timeline...")

    # Ensure time is in datetime format for processing
    df['tms'] = pd.to_datetime(df['tms'])

    # Identify breakpoints where state changes
    df['state_change'] = df['state'].ne(df['state'].shift())
    breakpoints = df[df['state_change']].copy()

    # Calculate duration for each state segment
    breakpoints['duration'] = breakpoints['tms'].diff().shift(-1)

    # Fill last segment duration with a fixed value (end of start to last segment)
    final_duration = df['tms'].iloc[-1] - breakpoints['tms'].iloc[-1]
    breakpoints.loc[breakpoints.index[-1], 'duration'] = final_duration

    # Prepare data for visualization
    timeline_data = {
        'phase': [],
        'state': [],
        'start_time': [],
        'duration': []
    }

    for idx, row in breakpoints.iterrows():
        timeline_data['phase'].append(row['phase']) if 'phase' in df.columns else timeline_data['phase'].append(0)
        timeline_data['state'].append(row['state'])
        timeline_data['start_time'].append(row['tms'])
        timeline_data['duration'].append(row['duration'])

     # Convert to DataFrame for easy visualization
    timeline_df = pd.DataFrame(timeline_data)
    timeline_df['end_time'] = timeline_df['start_time'] + timeline_df['duration']

    return timeline_df