# https://plotly.com/python-api-reference/
from plotly    import graph_objects as go
from plotly    import express       as px
from streamlit import streamlit     as st
from utilities import utilities     as ut


# metrics
def ttb_metric(data):
    try:
        val = data['ttb']
        fig = go.Figure(go.Indicator(
            mode   = "number+delta",
            value  = val,
            title  = {'text': "TTB"},
            delta  = {'reference': -1},
            domain = {'row': 0, 'column': 1},
        ))
        fig.update_layout(height=300)
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"TTB metric threw an error: {e}")


def dtb_metric(data):
    try:
        val = data['dtb']
        fig = go.Figure(go.Indicator(
            mode   = "number+delta",
            value  = val,
            title  = {'text': "DTB"},
            delta  = {'reference': -2},
            domain = {'row': 0, 'column': 1}
        ))
        fig.update_layout(height=300)
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"DTB metric threw an error: {e}")


def ptl_metric(data):
    try:
        val = data['ptl']
        fig = go.Figure(go.Indicator(
            value  = val,
            mode   = "number",
            title  = {'text': "PTL"},
            domain = {'row': 0, 'column': 1}
        ))
        fig.update_layout(height=300)
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"PTL metric threw an error: {e}")


def lbs_metric(data):
    try:
        val = data['lbs']
        fig = go.Figure(go.Indicator(
            value  = val,
            mode   = "number",
            title  = {'text': "LBS"},
            domain = {'row': 0, 'column': 1}
        ))
        fig.update_layout(height=300)
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"LBS metric threw an error: {e}")


def sog_metric(data):
    try:
        val = data['sog']
        fig = go.Figure(go.Indicator(
            value  = val,
            mode   = "number+delta",
            title  = {'text': "SOG"},
            delta  = {'reference': 5.6},
            domain = {'row': 0, 'column': 1}
        ))
        fig.update_layout(height=300)
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"SOG metric threw an error: {e}")


# plots
def ttb_plot(dataframe):
    try:
        dat = dataframe.copy()
        dat['ttb_seconds'] = dat['ttb'].apply(ut.start_timer_to_seconds)
        fig = px.line(
            dat,
            x = "timer",
            y = "ttb_seconds",
            title   = "TTB",
            range_x = [240, 360],
            range_y = [-10, 50],
            color_discrete_sequence = ['#0000FF']
        )
        # Define the tick values and custom labels
        vals = list(range(int(dat['ttb_seconds'].min()), int(dat['ttb_seconds'].max()) + 1, 10))
        text = [ut.seconds_to_start_timer(tick) for tick in vals]
        fig.update_layout(
            height=350,
            xaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            ),
            yaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            )
        )
        fig.update_yaxes(tickvals=vals, ticktext=text)
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"TTB threw an error: {e}")


def dtb_plot(dataframe):
    try:
        dat = dataframe.copy()
        fig = px.line(
            dat,
            x = "timer",
            y = "dtb",
            title = "DTB",
            range_x = [240, 360],
            range_y = [-10, 50],
            color_discrete_sequence = ['#FF0000']
        )
        fig.update_layout( 
            height=350,
            xaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            ),
            yaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            ))
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"DTB threw an error: {e}")


def dtl_plot(dataframe):
    try:
        dat = dataframe.copy()
        fig = px.line(
            dat,
            x = "timer",
            y = "dtl",
            title = "DTL",
            range_x = [240, 360],
            range_y = [-10, 50],
            color_discrete_sequence = ['#00FF00']
        )
        fig.update_layout(
            height=350,
            xaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            ),
            yaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            )
        )
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"DTL threw an error: {e}")


def sog_plot(dataframe):
    try:
        dat = dataframe.copy()
        fig = px.line(
            dat,
            x = "timer",
            y = "sog",
            title = "SOG",
            range_x = [240, 360],
            # range_y = [2, 7],
            color_discrete_sequence = ['#800080']
        )
        fig.update_layout(
            height=350,
            xaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            ),
            yaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            )
        )
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"SOG threw an error: {e}")


def start_gantt(dataframe):
    try:
        # Define the desired order of states
        state_order = [
            "jibing",
            "tacking",
            "starting",
            "acceleration",
            "holding",
            "positioning",  
            "planning"
        ]
        
        fig = px.timeline(
            dataframe,
            x_start="start_time",
            x_end="end_time",
            y="state",
            color="state",
            title="Timeline",
        )
        fig.update_yaxes(
            categoryorder="array",
            categoryarray=state_order
        )
        fig.update_layout(
            xaxis_title="Time",
            yaxis_title="State",
            showlegend=False,
            height=300
        )
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"GANTT threw an error: {e}")


def trc_plot(dataframe):
    """
    Create an animated plot showing boat position and trace
    Args:
        dataframe: DataFrame with columns ['timer', 'x', 'y']
    """
    try:
        dat = dataframe.copy()
        fig = go.Figure()

        startline_length = 300
        
        if {'xpt', 'ypt'}.issubset(dat.columns):
            gun_idx = dat[dat['timer'] == '00:00'].index[0]
            gun_xpt = dat['xpt'].iloc[gun_idx]
            gun_ptl = dat['ptl'].iloc[gun_idx]
            startline_length = ut.calculate_startline_length(gun_xpt, gun_ptl)

        # Store original timer values before converting to seconds
        original_timer = dat['timer'].copy()
        dat['timer'] = dat['timer'].apply(ut.start_timer_to_seconds)

        startline_bias = dat['lbs'].iloc[0].replace('°', '')  # Remove degree symbol
        layline_length = 150
        upwind_angle = 40
        
        pin_x, pin_y, pin_lx, pin_ly, cb_x, cb_y, cb_lx, cb_ly = ut.calculate_laylines(startline_bias, startline_length, layline_length, upwind_angle)

        # Add the starting line
        fig.add_trace(go.Scatter(
            x=[-startline_length/2, +startline_length/2],
            y=[0, 0],
            mode='lines',
            name='Starting Line',
            line=dict(color='black', width=3),
            hoverinfo='skip'
        ))

        # Add pin end layline
        fig.add_trace(go.Scatter(
            x=[pin_x, pin_lx],
            y=[pin_y, pin_ly],
            mode='lines',
            name='Pin Layline',
            line=dict(color='black', width=1, dash='dash'),
            hoverinfo='skip'
        ))

        # Add Stb End end layline
        fig.add_trace(go.Scatter(
            x=[cb_x, cb_lx],
            y=[cb_y, cb_ly],
            mode='lines',
            name='Stb Layline',
            line=dict(color='black', width=1, dash='dash'),
            hoverinfo='skip'
        ))

        if {'xpt', 'ypt'}.issubset(dat.columns):
            # Add boat trace (historical path)
            fig.add_trace(go.Scatter(
                x=dat['xpt'],
                y=dat['ypt'],
                mode='lines',
                name='Boat Trace',
                line=dict(color='blue', width=1),
                opacity=0.5
            ))

            # Add animated boat position
            fig.add_trace(go.Scatter(
                x=dat['xpt'],
                y=dat['ypt'],
                mode='markers',
                name='Boat Position',
                marker=dict(
                    symbol='diamond-tall',
                    size=16,
                    color='navy',
                    angle=0,  # Initial angle
                ),
                hovertemplate=(
                    '<b>Time: %{customdata[0]}</b><br>' +
                    '<b>Timer: %{customdata[1]}</b><br>' +
                    '<b>Position:</b><br>' +
                    '  X: %{x:.1f}m<br>' +
                    '  Y: %{y:.1f}m<br>' +
                    '<b>Metrics:</b><br>' +
                    '  SOG: %{customdata[2]:.1f}kts<br>' +
                    '  TTB: %{customdata[3]}<br>' +
                    '  DTL: %{customdata[4]:.0f}m<br>' +
                    '  PTL: %{customdata[5]:.0f}<br>' +
                    '<extra></extra>'
                ),
                customdata=list(zip(
                    dat['tms'],
                    original_timer,
                    dat['sog'],
                    dat['ttb'],
                    dat['dtl'].round(),
                    dat['ptl'].round()
                ))
            ))

        # Add pin end marker (red triangle)
        fig.add_trace(go.Scatter(
            x=[-startline_length/2],  # Pin end x-coordinate
            y=[0],               # Pin end y-coordinate
            mode='markers',
            name='Pin End',
            marker=dict(
                symbol='triangle-up',
                size=16,
                color='red',
            ),
            hoverinfo='skip'
        ))

        # Add starboard end marker (green triangle)
        fig.add_trace(go.Scatter(
            x=[startline_length/2],   # Starboard end x-coordinate
            y=[0],               # Starboard end y-coordinate
            mode='markers',
            name='Stb End',
            marker=dict(
                symbol='triangle-up',
                size=16,
                color='green',
            ),
            hoverinfo='skip'
        ))

        # Update layout
        fig.update_layout(
            title="Boat Position",
            height=700,
            showlegend=True,
            xaxis=dict(
                title="X (m)",
                range=[-200, +200],
                showline=True,
                linecolor='black',
                linewidth=1
            ),
            yaxis=dict(
                title="Y (m)",
                range=[-150, +50],
                showline=True,
                linecolor='black',
                linewidth=1
            ), 
            # Fixed slider configuration
            sliders=[dict(
                active=0,
                yanchor='top',
                xanchor='left',
                currentvalue=dict(
                    font=dict(size=16),
                    prefix='Time: ',
                    suffix='s',
                    visible=True,
                    xanchor='right',
                ),
                pad=dict(b=10, t=50),
                len=0.9,
                x=0.1,
                y=0,
                # ticklen=0,
                tickwidth=0,
                steps=[dict(
                    args=[[f'frame{k}'],
                        dict(mode='immediate',
                            frame=dict(duration=100, redraw=True),
                            transition=dict(duration=0))
                        ],
                    label=original_timer.iloc[k],  # Use original timer values
                    method='animate',
                ) for k in range(len(dat))]
            )],
            
            # Adjust play/pause buttons using only label spacing
            updatemenus=[
                dict(
                    type='buttons',
                    showactive=True,
                    x=+0.07, # Position for Play button
                    y=-0.13, # Moved lower (was -0.15)
                    yanchor='top',
                    direction='right',
                    pad=dict(r=10),
                    bgcolor='#FFFFFF',
                    bordercolor='#000000',
                    font=dict(size=14, color='#000000'),
                    buttons=[
                        dict(
                            label='▶     Play   ',  # Added extra spaces for padding
                            method='animate',
                            args=[None, dict(
                                frame=dict(
                                    duration=100,
                                    redraw=True
                                ),
                                fromcurrent=True,
                                mode='immediate',
                                transition=dict(
                                    duration=0,
                                    easing='linear'
                                )
                            )],
                        ),
                    ],
                ),
                dict(
                    type='buttons',
                    showactive=True,
                    x=+0.07, # Position for Pause button
                    y=-0.22, # Moved lower (was -0.15)
                    yanchor='top',
                    direction='right',
                    pad=dict(r=10),
                    bgcolor='#FFFFFF',
                    bordercolor='#000000',
                    font=dict(size=14, color='#000000'),
                    buttons=[
                        dict(
                            label='⏸  Pause  ',  # Added extra spaces for padding
                            method='animate',
                            args=[[None], dict(
                                frame=dict(duration=0, redraw=False),
                                mode='immediate',
                                transition=dict(duration=0)
                            )],
                        ),
                    ],
                )
            ]
        )

        # Create animation frames
        if {'xpt', 'ypt'}.issubset(dat.columns):
            fig.frames = [
                go.Frame(
                    data=[
                        # Starting line (static)
                        go.Scatter(
                            x=[-startline_length/2, startline_length/2],
                            y=[0, 0],
                            mode='lines',
                            name='Starting Line',
                            line=dict(color='black', width=3),
                            hoverinfo='skip'
                        ),
                        # Pin end layline (static)
                        go.Scatter(
                            x=[pin_x, pin_lx],
                            y=[pin_y, pin_ly],
                            mode='lines',
                            name='Pin Layline',
                            line=dict(color='black', width=1, dash='dash'),
                            hoverinfo='skip'
                        ),
                        # Stb End end layline (static)
                        go.Scatter(
                            x=[cb_x, cb_lx],
                            y=[cb_y, cb_ly],
                            mode='lines',
                            name='Stb Layline',
                            line=dict(color='black', width=1, dash='dash'),
                            hoverinfo='skip'
                        ),
                        # Historical trace up to current time
                        go.Scatter(
                            x=dat['xpt'][:k+1],
                            y=dat['ypt'][:k+1],
                            mode='lines',
                            name='Boat Trace',
                            line=dict(color='blue', width=1),
                            opacity=0.5
                        ),
                        # Current boat position
                        go.Scatter(
                            x=[dat['xpt'].iloc[k]],
                            y=[dat['ypt'].iloc[k]],
                            mode='markers',
                            name='Boat Position',
                            marker=dict(
                                symbol='diamond-tall',
                                size=16,
                                color='navy',
                                angle=ut.calculate_boat_angle(dat['xpt'].values, dat['ypt'].values, k),
                            ),
                            hovertemplate=(
                                '<b>Time: %{customdata[0]}</b><br>' +
                                '<b>Timer: %{customdata[1]}</b><br>' +
                                '<b>Position:</b><br>' +
                                '  X: %{x:.1f}m<br>' +
                                '  Y: %{y:.1f}m<br>' +
                                '<b>Metrics:</b><br>' +
                                '  SOG: %{customdata[2]:.1f}kts<br>' +
                                '  TTB: %{customdata[3]}<br>' +
                                '  DTL: %{customdata[4]:.0f}m<br>' +
                                '  PTL: %{customdata[5]:.0f}<br>' +
                                '<extra></extra>'
                            ),
                            customdata=[[
                                dat['tms'].iloc[k],
                                original_timer.iloc[k],
                                dat['sog'].iloc[k],
                                dat['ttb'].iloc[k],
                                dat['dtl'].iloc[k].round(),
                                dat['ptl'].iloc[k].round()
                            ]]
                        ),
                        # Pin end marker
                        go.Scatter(
                            x=[-startline_length/2],
                            y=[0],
                            mode='markers',
                            name='Pin End',
                            marker=dict(
                                symbol='triangle-up',
                                size=16,
                                color='red',
                            ),
                            hoverinfo='skip'
                        ),
                        # Starboard end marker
                        go.Scatter(
                            x=[startline_length/2],
                            y=[0],
                            mode='markers',
                            name='Stb End',
                            marker=dict(
                                symbol='triangle-up',
                                size=16,
                                color='green',
                            ),
                            hoverinfo='skip'
                        ),
                    ],
                    name=f'frame{k}'
                )
                for k in range(len(dat))
            ]

        # Display in Streamlit
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"Trace plot threw an error: {e}")
        st.error(f"Unable to display trace plot: {e}")