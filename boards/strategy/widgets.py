# https://plotly.com/python-api-reference/
from plotly    import graph_objects as go
from plotly    import express       as px
from streamlit import streamlit     as st
from utilities import utilities     as ut


# plots
def cat_plot(dataframe):
    try:
        # Create a categorical plot for wind category
        dat = dataframe.copy()
        dat['tms'] = ut.format_tms(dat['tms'])
        dat['tms_end'] = ut.format_tms_end(dat['tms'])
        
        # Define custom colors for wind categories
        cat_colors = {
            'Q1-LGW': '#3498DB',  # Blue
            'Q2-LGW': '#9B59B6',  # Purple
            'Q3-SGW': '#E74C3C',  # Red
            'Q4-SGW': '#1ABC9C',  # Teal
            'Q1-NSB': '#F1C40F',  # Yellow
            'Q1-BSB': '#E67E22',  # Orange
            'Q2-ASB': '#2ECC71',  # Green
            'Q2-LSB': '#34495E',  # Dark Gray
        }

        fig = px.timeline(
            dat, 
            x_start="tms",
            x_end="tms_end",
            y="cat", 
            color="cat",
            color_discrete_map=cat_colors,
            title="WIND CATEGORY",
            height=350
        )
        fig.update_layout(
            height=350,
            xaxis_title="Timestamp",
            yaxis_title="Category",
            xaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            ),
            yaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            ),
            showlegend=True, 
            bargroupgap=0.1,
            bargap=0.7
        )
        # Update traces to make bars more visible
        fig.update_traces(
            marker_line_color='black',
            marker_line_width=1,
            opacity=0.8
        )
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"CAT threw an error: {e}")


def str_plot(dataframe):
    try:
        # Create a categorical plot for wind strategy
        dat = dataframe.copy()
        dat['tms'] = ut.format_tms(dat['tms'])
        dat['tms_end'] = ut.format_tms_end(dat['tms'])
        
        # Define custom colors for wind strategies
        str_colors = { 
            'STN': '#2C3E50',  # Midnight Blue
            'PTS': '#E67E22',  # Burnt Orange
            'SBE': '#BDC3C7',  # Silver
        }

        fig = px.timeline(
            dat, 
            x_start="tms", 
            x_end="tms_end",
            y="str", 
            color="str",
            color_discrete_map=str_colors,
            title="WIND STRATEGY",
            height=350
        )
        fig.update_layout(
            height=350,
            xaxis_title="Timestamp",
            yaxis_title="Strategy",
            xaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            ),
            yaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            ),
            showlegend=True,
            bargroupgap=0.1,
            bargap=0.7,
        )
        # Update traces to make bars more visible
        fig.update_traces(
            marker_line_color='black',
            marker_line_width=1,
            opacity=0.8
        )
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"STR threw an error: {e}")


def gwd_plot(dataframe):
    try:
        # Create a line plot for wind angle
        dat = dataframe.copy()
        fig = px.line(
            dat, 
            x="tms", 
            y="gwd",
            title="GWD",
            color_discrete_sequence = ['#E67E22']
        )
        fig.update_layout(
            height=350,
            xaxis_title="Timestamp",
            yaxis_title="GWD (°)",
            xaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            ),
            yaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            )
        )
        fig.update_yaxes(ticksuffix="°")
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"GWA threw an error: {e}")


def gwa_plot(dataframe):
    try:
        # Create a line plot for wind angle
        dat = dataframe.copy()
        fig = px.line(
            dat, 
            x="tms", 
            y="gwa",
            title="GWA",
            # range_y = [-180, +180],
            color_discrete_sequence = ['#2E86C1']
        )
        fig.update_layout(
            height=350,
            xaxis_title="Timestamp",
            yaxis_title="GWA (°)",
            xaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            ),
            yaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            )
        )
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"GWA threw an error: {e}")


def gws_plot(dataframe):
    try:
        # Create a line plot for wind speed
        dat = dataframe.copy()
        fig = px.line(
            dat, 
            x="tms", 
            y="gws",
            title="GWS",
            # range_y = [0, 25],
            color_discrete_sequence = ['#8E44AD']
        )
        fig.update_layout(
            height=350,
            xaxis_title="Timestamp",
            yaxis_title="GWS (kts)",
            xaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            ),
            yaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            )
        )
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"GWS threw an error: {e}")


def dst_plot(dataframe):
    try:
        # Create a line plot for dst parameter
        dat = dataframe.copy()
        fig = px.line(
            dat, 
            x="tms", 
            y="dst",
            title="DST",
            # range_y = [-10, +10],
            color_discrete_sequence = ['#27AE60']
        )
        fig.update_layout(
            height=350,
            xaxis_title="Timestamp",
            yaxis_title="DST",
            xaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            ),
            yaxis=dict(
                showline=True,
                linecolor='black',
                linewidth=1
            )
        )
        st.plotly_chart(fig, use_container_width=True)
    except Exception as e:
        print(f"DST threw an error: {e}")