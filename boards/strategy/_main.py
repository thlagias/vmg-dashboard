from streamlit import streamlit as st
from utilities import utilities as ut
from widgets   import widgets   as wg
from agents    import strategy  as ai


def show_dashboard():
    col1, col2 = st.columns([2, 2])  # Adjust column width ratio as needed
    # Load CSS
    with open('static/styles.css') as f:
        st.markdown(f'<style>{f.read()}</style>', unsafe_allow_html=True)
    with col1:
        st.title("🧭 Strategy Analysis") # Set dashboard title
    with col2:
        forecast_file = st.file_uploader("Upload csv file", key="forecast_uploader")
    
    if forecast_file is not None:
        try:
            df = ut.load_data(forecast_file)
            # category, strategy, gwd, gwa, gws, diffSurfTemp plots
            col1, col2 = st.columns(2)
            with col1:
                wg.cat_plot(df)
                wg.gwd_plot(df)
                wg.gws_plot(df)
            with col2:
                wg.str_plot(df)
                wg.gwa_plot(df)
                wg.dst_plot(df)

            # dataframe
            with st.expander("Data Preview"):
                st.dataframe(df, use_container_width=True, height=500)
        except Exception as e:
            print(f"An error occurred: {e}")