# modules
from streamlit import streamlit as st
from utilities import utilities as ut
from boards.yawing import widgets as wg
from boards.yawing import analysis as da


def show_dashboard():
    # Load CSS
    with open('static/styles.css') as f:
        st.markdown(f'<style>{f.read()}</style>', unsafe_allow_html=True)

    # Dashboard title
    st.markdown("<h1 style='text-align: center;'>⛵ Maneuver Analysis</h1>", unsafe_allow_html=True)