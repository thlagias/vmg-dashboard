# VMG-Dashboard


## Overview
VMG-Dashboard is a sailing analytics platform built with Streamlit to analyze and visualize racing data.

The system features:
- Multi-dimensional analysis through specialized dashboards (Race, Start, Strategy, Maneuvers, Performance)
- CrewAI-powered intelligent analysis agents for racing, starting, and strategy
- Advanced data processing pipeline with staged data management
- Interactive visualizations and time series analysis
- Real-time performance metrics and analytics
- Statistical analysis tools and pattern recognition
- Support for both Windows and macOS environments

The dashboard serves as a central hub for: 
- race analysis, 
- performance optimization
- strategic decision-making

## Table of Contents
- [Installation](#installation)
- [Usage](#usage)
- [Project Structure](#project-structure)
- [Available Dashboards](#available-dashboards)
- [Features](#features)
- [Useful Links](#useful-links)

## Installation
### Prerequisites
- Python 3.7+
- pip

### Steps
1. **Clone the repository:**
   ```bash
   git clone https://github.com/your-username/vmg-dashboard.git
   cd vmg-dashboard

2. **Create virtual environment:**
    ```bash
    python -m venv venv

3. **Activate virtual environment:**
    ```bash
    venv\Scripts\activate # windows
    source venv/bin/activate # macos

4. **Install dependencies:**
    ```bash
    pip install -r requirements.txt

5. **Deactivate virtual environment:**
    ```bash
    deactivate

6. **Resolve execution policy error:**
    ```bash
    Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser

### Usage
1. **Run the app:**
    ```bash
     # windows
    $env:ENV="<env>"
    streamlit run app.py
     # macos
    ENV=<env> streamlit run app.py

2. **Access the dashboard:**
    Open your web browser and go to  http://localhost:8501

### Project Structure
vmg-dashboard/  
├── agents/   
│   ├── racing  
│   ├── starting   
│   ├── strategy  
│   ├── velocity  
│   ├── yawing  
├── boards/   
│   ├── data  
│   ├── racing   
│   ├── starting  
│   ├── strategy  
│   ├── velocity  
│   ├── yawing  
├── datasets/  
|   ├── stage1  
│   ├── stage2  
│   ├── stage3   
├── knowledge/  
│   ├── race.txt  
│   ├── start.txt   
│   ├── wind.txt  
├── static/  
│   ├── styles.css  
├── utilities/  
│   ├── network.py    
│   ├── utilities.py  
├── app.py   
├── README.md   
├── requirements-macos.txt  
├── requirements-windows.txt  
└── todo.txt  
  
- agents/: Directory containing crewai agents for racing, starting, strategy, velocity, and yawing analysis
- boards/: Directory containing the main dashboard logic and visualizations for each view
- datasets/: Directory containing staged data files
- knowledge/: Directory containing knowledge base text files for race, start, and wind analysis
- static/: Directory for storing static files like CSS
- utilities/: Directory for utility modules and network functions
- app.py: The main Streamlit application file that handles dashboard routing
- README.md: This file
- requirements-macos.txt: File listing the required Python packages for macOS
- requirements-windows.txt: File listing the required Python packages for Windows
- tasklist.txt: File containing pending tasks and improvements


### Available Dashboards

1. **Data Import Dashboard**
- Data availability table (raceId, weather/start/race file status)
- File import and manipulation interface
- Data cleaning tools for race, start, and strategy data
- Event detection and state definition
- Data staging management

2. **Race Overview Dashboard**
- Race performance metrics
- Race timeline visualization (TWA Gantt chart)
- Multiple time series plots (VMG, SOG, TWA, TWS, TWD)
- Interactive race viewer
- Statistical analysis tools
    - Descriptive statistics
    - Correlation analysis
    - CrewAI race analysis agent

3. **Start Analysis Dashboard**
- Start performance metrics
- Start timeline visualization (TWA Gantt)
- Pre-start performance plots
    - SOG analysis
    - Distance to line (DTL)
    - Time to burn (TTB)
    - Distance to burn (DTB)
- Interactive start viewer
- CrewAI agents for start analysis and decision-making

4. **Strategy Analysis Dashboard**
- Strategy metrics (TWD, TWS, GWD, GWS averages)
- Wind direction and classification Gantt charts
- Strategy type timeline
- Per-leg strategy viewer
- Advanced analysis tools
    - Statistical and correlation analysis
    - Time series predictions (ARIMA)
    - Wind pattern analysis
    - Seasonal decomposition
    - CrewAI strategy analysis agent

5. **Maneuver Analysis Dashboard**
- Maneuver metrics (tacks/jibes counts and performance)
- Maneuver timeline visualization
- Per-leg maneuver analysis
- Statistical tools and CrewAI maneuver analysis

6. **Performance Analysis Dashboard**
- Performance metrics and ratios
- Performance Gantt visualization
- Comparative performance plots
    - VMG vs Target VMG
    - SOG vs Polar performance
- Per-leg performance viewer
- Statistical analysis tools with CrewAI integration

### Features
- Dynamic Graphs and Charts: Visualize data using various types of graphs and charts.
- User Inputs: Custom filters and queries to manipulate the displayed data.
- Real-time Updates: Interactive elements that update the visualizations in real-time based on user input.
- Data Processing: Functions for cleaning, processing, and aggregating data before visualization.

### Technology Stack
- **Streamlit**: Web application framework for the dashboard
- **Plotly**: Data visualization library for interactive charts
- **Pandas**: Data manipulation and analysis
- **NumPy**: Numerical computing tools
- **Python 3.7+**: Base programming language

### Useful Links
- https://streamlit.io/docs
- https://plotly.com/python-api-reference/
- https://plotly.com/python/indicator/
- https://plotly.com/python/animations/
- https://plotly.com/python/time-series/
