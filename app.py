# libraries
import os
# modules
from dotenv import load_dotenv as env
from streamlit import streamlit as st


# load environment variables
env_name = os.getenv("ENV", "dev")
env_file = f"{env_name}.env"
env(env_file)

# Set page layout
st.set_page_config(layout="wide")

# Define page navigation functions
def set_page(page_name):
    st.session_state.current_page = page_name

# Set sidebar layout
with st.sidebar:
    st.button("📁 Data Import", on_click=set_page, args=("data",), use_container_width=True)
    st.button("🏁 Race Overview", on_click=set_page, args=("race",), use_container_width=True)
    st.button("🚩 Start Analysis", on_click=set_page, args=("start",), use_container_width=True)
    st.button("🧭 Strategy Analysis", on_click=set_page, args=("strategy",), use_container_width=True)
    st.button("⛵ Maneuver Analysis", on_click=set_page, args=("maneuver",), use_container_width=True)
    st.button("📊 Performance Analysis", on_click=set_page, args=("performance",), use_container_width=True)

# Initialize session state
if 'current_page' not in st.session_state:
    st.session_state.current_page = "data"

# Main Content Area
if st.session_state.current_page == "data":
    from boards.data import _main
    _main.show_dashboard()
elif st.session_state.current_page == "race":
    from boards.racing import _main
    _main.show_dashboard()
elif st.session_state.current_page == "start":
    from boards.starting import _main
    _main.show_dashboard()
elif st.session_state.current_page == "strategy":
    from boards.strategy import _main
    _main.show_dashboard()
elif st.session_state.current_page == "maneuver":
    from boards.yawing import _main
    _main.show_dashboard()
elif st.session_state.current_page == "performance":
    from boards.velocity import _main
    _main.show_dashboard()
