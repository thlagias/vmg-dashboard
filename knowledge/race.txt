# The CSV file contains a time-series dataset collected during a sailing race. 
  Here is a brief overview of each column and the data it includes:

- tms: Represents the exact Timestamp the data row was recorded.
    This is in ISO 8601 date-time string with UTC time (YYYY-MM-DDTHH:MM:SSZ).
- timer: Represents a stopwatch-style Timer used during the race.
    This is in MM:SS format.
- tws: Represents the True Wind Speed.
    This is a numeric value with one decimal point, typically in knots.
- twd: Represents the True Wind Direction relative to true north.
    This is a numeric value with no decimal points in Degrees (°).
- twa: Represents the True Wind Angle between the boat's heading and true wind direction.
    This is a numeric value with no decimal points in Degrees (°).
    On starboard tack takes values in range [0, +180].
    On port tack takes values in range [0, -180]
- cog: Represents the sailboat Course Over Ground relative to true north.
    This is a numeric value with no decimal points in Degrees (°).
- sog: Represents the sailboats's Speed Over Ground.
    This is a numeric value with one decimal point, typically in knots.
- accel: Represents the sailboat's acceleration.
    This is a numeric value with one decimal point, measured in knots per second.
- vmg: Represents the sailboat's Velocity Made Good.
    Vmg is the component of the sailboat's velocity in the direction of the wind.
    This is a numeric value with one decimal point, typically in knots.
- sft: Represents the amount of wind shift over time. This field is not really used.
    This is a numeric value in Degrees (°). Receives also negative values.
- xpt: Represents the x-coord of the sailboat's position relative to the starting line middle.
    The reference point is the middle of the starting line.
    This is a numeric value with one decimal point.
- ypt: Represents the y-coord of the sailboat's position relative to the starting line middle.
    This is a numeric value with one decimal point.
- state: Represents the current state which can be one of the 6 Types described below.


# The entire data of a sailing race can be split into States.
Each State can be one of the following 6 State Types:
A.Sailing State Types: 
    1.upwind, 
    2.downwind, 
    3.reaching
B.Maneuver State Types: 
    4.tacking, 
    5.jibing, 
    6.rounding


# The entire data of a sailing race can be split also into Legs.
Each Leg can be one of the following 3 Leg Types based on the sailing States included:
    1.upwing, 
    2.downwind, 
    3.reaching


# A Leg is the time slot between:
    - the end timestamp of the last mark rounding
    - the start timestamp of the next mark rounding
