# libraries
import os as os
import requests as rq
# modules
from pathlib import Path as Path


def list_races():
    try:
        # Retrieve API URL and API key from environment variables
        api_url = os.getenv("VMGINS_API_URL")
        api_key = os.getenv("VMGINS_API_KEY")
        headers = {"x-api-key": api_key}

        # API call to get a list of all raceIds and their files
        list_endpoint = f"{api_url}/list"
        list_response = rq.get(list_endpoint, headers=headers)
        list_race_ids = list_response.json()["races"]

        return list_race_ids
    except Exception as e:
        print(f"Error fetching races: {e}")
        return {}
    

def get_race_presigned_urls(raceId):
    try:
        # Retrieve API URL and API key from environment variables
        api_url = os.getenv("VMGINS_API_URL")
        api_key = os.getenv("VMGINS_API_KEY")
        headers = {"x-api-key": api_key}

        # API calls to get presigned URLs for each file type
        class_endpoint = f"{api_url}/class?raceId={raceId}"
        class_response = rq.get(class_endpoint, headers=headers)
        class_presign  = class_response.json()["url"]

        start_endpoint = f"{api_url}/start?raceId={raceId}"
        start_response = rq.get(start_endpoint, headers=headers)
        start_presign  = start_response.json()["url"]

        race_endpoint = f"{api_url}/race?raceId={raceId}"
        race_response = rq.get(race_endpoint, headers=headers)
        race_presign  = race_response.json()["url"]

        presigned_urls = {
            "raceId": raceId,
            "class" : class_presign,
            "start" : start_presign,
            "race"  : race_presign
        }
        
        return presigned_urls
    except Exception as e:
        print(f"Error getting presigned URLs: {e}")
        return None
    

def get_race_files(presigned_urls):
    try:
        raceId = presigned_urls["raceId"]
        racedir = Path("datasets/stage1")

        # Download class file
        class_endpoint = presigned_urls["class"]
        class_response = rq.get(class_endpoint)
        class_response.raise_for_status()
        with open(racedir / f"{raceId}-class-data.csv", 'wb') as f:
            f.write(class_response.content)

        # Download start file
        start_endpoint = presigned_urls["start"]
        start_response = rq.get(start_endpoint)
        start_response.raise_for_status()
        with open(racedir / f"{raceId}-start-data.csv", 'wb') as f:
            f.write(start_response.content)

        # Download race file
        race_endpoint = presigned_urls["race"]
        race_response = rq.get(race_endpoint)
        race_response.raise_for_status() 
        with open(racedir / f"{raceId}-race-data.csv", 'wb') as f:
            f.write(race_response.content)

        return True
    except Exception as e:
        print(f"Error downloading files: {e}")
        return False
