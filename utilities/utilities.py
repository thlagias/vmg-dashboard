# libraries
import math as ma
import numpy as np
import pandas as pd


# Function to load a csv file
def load_data(file):
    data = pd.read_csv(file)
    return data


# Function to normalize an angle 
def n_degrees(num):
    if num <   0: num += 360
    if num > 360: num -= 360
    return num


# Function to calculate average value
def mean_val(nums):
    return sum(nums) / len(nums)


# Function to calculate average direction
def mean_dir(angles):
    sin_sum = 0
    cos_sum = 0
    for angle in angles:
        angle_rad = ma.radians(angle)
        sin_sum += ma.sin(angle_rad)
        cos_sum += ma.cos(angle_rad)
    mean_rads = ma.atan2(sin_sum, cos_sum)
    mean_degs = ma.degrees(mean_rads)
    return n_degrees(mean_degs)


# Function to convert MM:SS or -MM:SS to total seconds
def start_timer_to_seconds(time_str):
    if pd.isna(time_str):
        return None
    sign = -1 if time_str.startswith('-') else 1
    time_str = time_str.strip('-')
    minutes, seconds = map(int, time_str.split(':'))
    total_seconds = sign * (minutes * 60 + seconds)
    return total_seconds


# Function to convert total seconds back to MM:SS format
def seconds_to_start_timer(seconds):
    sign = '-' if seconds < 0 else ''
    seconds = abs(seconds)
    minutes, seconds = divmod(seconds, 60)
    return f'{sign}{int(minutes):02}:{int(seconds):02}'


# Convert race timer MM:SS format to total seconds.
def race_timer_to_seconds(timer):
    minutes, seconds = map(int, timer.split(':'))
    return minutes * 60 + seconds


# Convert total seconds to race timer format (HH:MM:SS or MM:SS).
def seconds_to_race_timer(secs):
    hours = secs // 3600
    minutes = (secs % 3600) // 60
    seconds = secs % 60
    return f"{hours:02d}:{minutes:02d}:{seconds:02d}" if hours > 0 else f"{minutes:02d}:{seconds:02d}"


# Helper function to format TTB string
def format_ttb(ttb_str):
    try:
        # Handle negative times
        is_negative = ttb_str.startswith('-')
        ttb_str = ttb_str.replace('-', '')
        
        # Split minutes and seconds
        minutes, seconds = map(int, ttb_str.split(':'))
        formatted = f"{minutes:02d}:{seconds:02d}"
        
        return f"-{formatted}" if is_negative else formatted
    except:
        return "00:00"  # fallback value
    

# Helper function to format directions
def format_dir(value):
    try:
        if pd.isna(value):
            return "N/A"
        if isinstance(value, str):
            # Remove degree symbol if present and convert to float
            value = float(value.replace('°', ''))
        return f"{value:.0f}"
    except:
        return "N/A"

  
# Helper function to format tms column
def format_tms(tms):
    return pd.to_datetime(tms)


# Helper function to format tms threshold column
def format_tms_end(tms):
    return tms + pd.Timedelta(minutes=40)


# Helper function to calculate average of spd columns
def mean_spd(col):
    return round(col.mean(), 1)


# Helper function to calculate std dev of spd columns
def stdv_spd(col):
    return round(col.std(), 1)


# Helper function to calculate average of twa columns
def mean_twa(col):
    return round(col.abs().mean(), 0)


# Helper function to calculate std dev of twa columns
def stdv_twa(col):
    return round(col.abs().std(), 1)


# Helper function to average twd columns
def mean_twd(col):
    return round(col.str.replace('°', '').astype(float).mean(), 0)


# Helper function to calculate std dev of twd columns
def stdv_twd(col):
    return round(col.str.replace('°', '').astype(float).std(), 0)


# Helper function to calculate boat angle based on movement direction with smoothing
def calculate_boat_angle(x_coords, y_coords, k):
    if k > 1:  # Need at least 2 previous points for better direction
        # Calculate direction using last 2 positions for more stability
        dx = x_coords[k] - x_coords[k-1]
        dy = y_coords[k] - y_coords[k-1]
        
        # Calculate angle in degrees
        angle = np.degrees(np.arctan2(dy, dx))
        
        # Convert mathematical angle to compass heading
        heading = 90 - angle  # This is the key transformation
        
        # Normalize heading to 0-360 range
        heading = heading % 360
        
        return heading
    return 0  # Default angle for first position


# Calculate layline endpoints for both ends of the starting line
def calculate_laylines(line_bias, line_length, layline_length, upwind_angle):
    # Clean and convert line_bias to float
    try:
        line_bias = float(line_bias.replace('°', ''))
    except (TypeError, ValueError, AttributeError):
        print(f"Warning: Invalid line bias value: {line_bias}, using 0")
        line_bias = 0.0
        
    # Calculate starboard and port angles as in Java
    stb_angle = upwind_angle - line_bias  # Starboard angle
    
    # Convert to radians and calculate dx, dy
    stb_rad = np.radians(stb_angle)
    dx = layline_length * np.sin(stb_rad)
    dy = layline_length * np.cos(stb_rad)
    
    # Calculate layline points for pin end (left side)
    pin_x = -line_length/2
    pin_y = 0
    pin_layline_x = pin_x + dx  # Use dx directly as in Java
    pin_layline_y = pin_y - dy  # Negative dy as in Java
    
    # Calculate layline points for committee boat end (right side)
    cb_x = line_length/2
    cb_y = 0
    cb_layline_x = cb_x + dx
    cb_layline_y = cb_y - dy
    
    return (pin_x, pin_y, pin_layline_x, pin_layline_y,
            cb_x, cb_y, cb_layline_x, cb_layline_y)


#  Calculate the starting line length based on the boat's position at the gun
def calculate_startline_length(xpt_at_gun, ptl_at_gun):
    # ptl of 5 means -startline_length/2
    # ptl of 25 means +startline_length/2
    # So xpt = startline_length/2 * (2*ptl - 30)/20
    # Therefore:
    # startline_length = 2 * xpt * 20/(2*ptl - 30)
    return abs(2 * xpt_at_gun * 20/(2*ptl_at_gun - 30))


# Add a continuous timer column that doesn't reset, preserving the original timer.
def add_continuous_timer(df):
    seconds = df["timer"].apply(race_timer_to_seconds)
    resets = seconds.diff() < 0
    total_seconds = seconds + (resets.cumsum() * 3600)
    df["timer"] = total_seconds.apply(seconds_to_race_timer)
    return df


# Convert MM:SS to HH:MM:SS, handling negative values
def hhmmss(x):
    if pd.isna(x): return x
    sign = '-' if x.startswith('-') else ''
    x = x.replace('-', '')
    if ':' in x:
        mm, ss = x.split(':')
        return f"{sign}00:{int(mm):02d}:{int(ss):02d}"
    return x