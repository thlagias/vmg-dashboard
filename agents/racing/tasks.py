####################### Performance Optimization Insights #######################
performance_optimization_description = (
    "Perform a detailed correlation analysis on the provided {data_file} file " 
    "to determine relationships between variables, focusing on performance metrics " 
    "(speed, wind angles, sail configurations, and tactical decisions). "
    "Limit the analysis to sailing states categorized as upwind, downwind, and reaching. " 
    "Exclude maneuver states (tacking, jibing, rounding) to avoid overlap with maneuver-specific tasks. "
    "Generate and present the entire correlation matrix for all these parameters " 
    "and report any key findings which can help to maximize boat speed and efficiency."
)

performance_optimization_output = (
    "A structured report detailing the main performance-influencing factors with correlation values, "
    "the entire correlation matrix for all these parameters, and key findings which can lead to "
    "potential gains in boat performance."
)


####################### Maneuver Optimization Insights #######################
maneuver_optimization_description = (
    "Analyze correlations on the provided {data_file} file to determine relationships between variables, " 
    "focusing on maneuver execution (tacking, jibing, rounding) and race time loss or gains. "
    "Pinpoint the maneuvers with the highest performance variability and identify patterns of successful " 
    "and unsuccessful executions. Provide the entire correlation matrix and actionable insights on " 
    "technique refinements for executing specific maneuvers."
)

maneuver_optimization_output = (
    "A report highlighting the maneuvers that introduce the greatest race time variability, "
    "detailed breakdown of maneuver success factors, and recommendations to minimize time loss. "
    "Includes a step-by-step improvement plan for the most critical maneuvers with estimated time savings."
)


########################## Race Optimization Insights ##########################
race_optimization_insights_description = (
    "Synthesize findings from performance and maneuver optimization analyses to create a cohesive " 
    "race report. Prioritize actionable insights that yield the highest probability of " 
    "improving race results. Identify trade-offs between different optimization paths."
)

race_optimization_insights_output = (
    "A comprehensive race optimization report including the entire "
    "performance optimization report and maneuver optimization report." 
    "It should also summarize key insights from performance and maneuver analyses. "
)
