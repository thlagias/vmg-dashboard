import os
import warnings
from . import tasks
from . import agents
from crewai.knowledge.source import (text_file_knowledge_source)
from crewai_tools import (CSVSearchTool)
from crewai import (Crew, Agent, Task, Process, LLM)



warnings.filterwarnings("ignore")
source_file = "../source/race.txt"
sample_file = "../export/race.csv"


#################################### LLMs ####################################
gpt4o = LLM(
    model = os.environ.get("OPENAI_MODEL_NAME"), 
    temperature = float(os.environ.get("OPENAI_MODEL_TEMP"))
)


############################## Knowledge Sources ##############################
txt_source = text_file_knowledge_source.TextFileKnowledgeSource(
    file_path = source_file,
    metadata = {
        "source_type": "text_file",
        "file_name": "race.txt",
        "tags": "sailing, boat, wind, maneuver",
        "description": "This text contains insights on User."
    }
)


#################################### Agents ####################################
performance_optimizer = Agent(
    llm = gpt4o,
    role = "Performance Optimizer",
    backstory = agents.performance_optimizer_story,
    goal = agents.performance_optimizer_goal,
    memory = False,
    verbose = True
)

maneuver_optimizer = Agent(
    llm = gpt4o,
    role = "Maneuver Optimizer",
    backstory = agents.maneuver_optimizer_story,
    goal = agents.maneuver_optimizer_goal,
    memory = False,
    verbose = True
)

sailing_coach = Agent(
    llm = gpt4o,
    role = "Sailing Coach",
    backstory = agents.sailing_coach_story,
    goal = agents.sailing_coach_goal,
    memory = False,
    verbose = True
)


#################################### Tasks ####################################
performance_optimization = Task(
    agent = performance_optimizer,
    description = tasks.performance_optimization_description,
    expected_output = tasks.performance_optimization_output,
)

maneuver_optimization = Task(
    agent = maneuver_optimizer,
    description = tasks.maneuver_optimization_description,
    expected_output = tasks.maneuver_optimization_output,
)

race_optimization = Task(
    agent = sailing_coach,
    context = [
        performance_optimization,
        maneuver_optimization
    ],
    description = tasks.race_optimization_insights_description,
    expected_output = tasks.race_optimization_insights_output,
    output_file = "./export/report.md"
)


#################################### Crew ####################################
crew = Crew(
    agents = [
        performance_optimizer,
        maneuver_optimizer,
        sailing_coach
    ],
    tasks = [
        performance_optimization,
        maneuver_optimization,
        race_optimization
    ],
    process = Process.sequential,
    verbose = True,
    full_output = True,
    knowledge_sources = [txt_source]
)


def kickoff():
    csv_tool = CSVSearchTool(csv=sample_file)
    performance_optimizer.tools = [csv_tool]
    maneuver_optimizer.tools = [csv_tool]
    inputs = {"data_file":"data.csv"}
    crew.kickoff(inputs=inputs)